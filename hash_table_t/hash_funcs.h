#ifndef hash_funcs_h
#define hash_funcs_h

#define HASH_FUNCS_AMOUNT 6

typedef uint64_t (*hash_func_t)( char* const );

u_int64_t hash_const( char* const str );

u_int64_t hash_fsymb( char* const str );

u_int64_t hash_len( char* const str );

u_int64_t hash_sum( char* const str );

u_int64_t hash_ror( char* const str );

u_int64_t hash_gnu( char* const str );

extern const hash_func_t hash_funcs[];

#endif // hash_funcs_h

