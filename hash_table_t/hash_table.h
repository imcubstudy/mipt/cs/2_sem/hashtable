#ifndef __hash_table_h__
#define __hash_table_h__

#include "hash_funcs.h"
#include "containers.h"

#define DEFAULT_HTABLE_SIZE     100

using namespace vl_containers;

struct hash_table
{
    list_t**        data;
    size_t          size;
    hash_func_t     hfunc;
};

hash_table*     construct_htable( size_t size, hash_func_t hfunc, size_t def_capacity );
bool            destruct_htable( hash_table* htable );

bool            push_str( hash_table* htable, char* const str );

bool            csv_dump( hash_table* htable, FILE *file );

bool            contains( hash_table* htable, char* const str );

#endif // __hash_table_h__
