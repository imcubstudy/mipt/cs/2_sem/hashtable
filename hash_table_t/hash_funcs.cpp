#include <stdint.h>
#include <cstring>
#include "hash_funcs.h"

const hash_func_t hash_funcs[] = {
    hash_const,
    hash_fsymb,
    hash_len,
    hash_sum,
    hash_ror,
    hash_gnu
};

u_int64_t hash_const( char* const str )
{
    return 1;
}

u_int64_t hash_fsymb( char* const str )
{
    return *str;
}

u_int64_t hash_len( char* const str )
{
    return strlen( str );
}

u_int64_t hash_sum( char* const str )
{
    size_t     len     = strlen( str );
    
    u_int64_t result = 0;
    
    for( size_t i = 0; i < len; ++i )
    {
        result += str[i];
    }
    return result;
}

u_int64_t hash_ror( char* const str )
{
    u_int64_t hash = 0;
    
    char* ptr = str;
    
    while(*ptr != '\0'){
        if(hash % 2 == 0)
            hash = hash >> 1;
        else{
            hash = hash >> 1;
            hash += (1 << 31); // 2^31
        }
        
        hash = hash ^ (*ptr++);
    }
    return hash;
}

u_int64_t hash_gnu( char* const str)
{
    u_int64_t hash = 5381;
    int c;
    
    char* ptr = str;
    
    while ((c = *ptr++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    
    return hash;
}
