#include <cstdint>
#include <cstring>
#include "containers.h"

#define ASM_OPTIMIZATION

using namespace vl_containers;

struct htable_elem_t
{
    char*   const       str     = nullptr;
    uint64_t            hidden  = UINT64_MAX;
};

int cmp_ht_elem( const void* ap, const void* bp )
{
#ifndef ASM_OPTIMIZATION
    htable_elem_t*      a   = (htable_elem_t*)ap;
    htable_elem_t*      b   = (htable_elem_t*)bp;

    if( a->hidden != b->hidden )
        return (int)a->hidden - (int)b->hidden;
    else return strcmp( a->str, b->str );
#else
    if( ((htable_elem_t*)ap)->hidden != ((htable_elem_t*)bp)->hidden )
        return 1;

    
    int     result = 0;
    __asm__ __volatile__
    (
        "___LOOP:                      ;\n\t"
        "lodsb                         ;\n\t"
        "test      %%al, %%al          ;\n\t"
        "jz        ___EQU              ;\n\t"
        "xorb      (%%rdi), %%al       ;\n\t"
        "jnz       ___NEQU             ;\n\t"
        "inc       %%rdi               ;\n\t"
        "jmp       ___LOOP             ;\n\t"
    
        "___EQU:   inc %[res]          ;\n\t"
        "___NEQU:                      ;\n\t"
    
        : [res]"=b" (result)
        : [a]"S" ((char*)ap), [b]"D" ((char*)bp)
        :
    );
    
    return result;
#endif // ASM_OPTIMIZATION
}

int destruct_ht_elem( void* ) { return 0; }

int copy_ht_elem( void* dest, const void* src )
{
    memcpy( dest, src, sizeof(htable_elem_t) );
    return 0;
}

data_funcs_t htable_elem_funcs = {
    cstring_default_funcs.print_func,
    cmp_ht_elem,
    copy_ht_elem,
    destruct_ht_elem
};

inline uint64_t hidden_hash( char* const str )
{
    size_t      len     = strlen( str );
    uint64_t    hidden  = len;
    
    uint64_t*   breakpoint = len * sizeof(char)/sizeof(uint64_t) + (uint64_t*)str;
    for( uint64_t* i = (uint64_t*)str; i < breakpoint; ++i )
        hidden  += *i;
    
    return hidden;
}

#include "hash_table.h"

hash_table*     construct_htable( size_t size, hash_func_t hfunc, size_t def_capacity )
{
    assert( hfunc != nullptr );
    
    hash_table*     res = (hash_table*)calloc( sizeof(hash_table), 1 );
    if( res == nullptr ) return res;
    
    res->hfunc  = hfunc;
    
    res->data   = (list_t**)calloc( sizeof(list_t*), size );
    if( res->data == nullptr )
    {
        free( res );
        return nullptr;
    }
    
    res->size = size;
    
    htable_elem_t poison = { nullptr, UINT64_MAX };
    
    for( size_t i = 0; i < res->size; ++i )
    {
        res->data[i]    = construct_list( "htable elem", sizeof( htable_elem_t ),
                                         htable_elem_funcs, &poison, def_capacity );
        
        if( res->data[i] == nullptr )
        {
            for( size_t j = 0; j < i; ++j )
                destruct_list( res->data[j] );
            free( res->data );
            free( res );
            
            return nullptr;
        }
    }
    
    return res;
}

bool destruct_htable( hash_table* htable )
{
    for( size_t j = 0; j < htable->size; ++j )
        destruct_list( htable->data[j] );
    free( htable->data );
    free( htable );
    
    return true;
}

bool push_str( hash_table* htable, char* const str )
{
    assert( str != nullptr );
    
    uint64_t      hidden = hidden_hash( str );
    
    htable_elem_t elem = { str, hidden };
    
    size_t idx = htable->hfunc( str ) % htable->size;
    
    if( find_by_value( htable->data[idx], &elem) == -1 )
    {
        int dummy = 0;
        lst_push_t( htable->data[idx], &elem, &dummy );
        return true;
    }
    
    return false;
}

bool    csv_dump( hash_table *table, FILE *file )
{
    assert( table != nullptr );
    assert( file != nullptr );
    
    for( size_t i = 0; i < table->size; ++i )
    {
        fprintf( file, "%zd; %zd\n", i, lst_size( table->data[i] ) );
    }
    return true;
}

bool    contains( hash_table* htable, char* const str )
{
    uint64_t        hidden  = hidden_hash( str );
    htable_elem_t   elem = { str, hidden };
    
    size_t idx = htable->hfunc( str ) % htable->size;
    
    if( find_by_value( htable->data[idx], &elem) != -1 )
        return true;
    
    return false;
}
