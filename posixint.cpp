#include <stdio.h>
#include <cstring>

#include "posixint.h"
#include "hash_table.h"

char    usage[] = "hashtest\n"
    "-f --file=[input file name]\n"
    "-h --hash=[hash function idx(s) {0-6}] ( ignored if all specified )\n"
    "-a --all - use all known hash funcs\n"
    "-r --rus - consider text file as cyrillic text ( default eng )\n"
    "-s --size=[hash table size(=100)]\n"
    "-d --default=[default list capacity(=100)]\n"
    "-e --epoch=[scan for each word(=0)]\n";

char    optstring[]     = "f:h:ars:d:e:";

option  program_options[] = {
    { "file",   required_argument, 0, 'f' },
    { "hash",   required_argument, 0, 'h' },
    { "all",    no_argument,       0, 'a' },
    { "rus",    no_argument,       0, 'r' },
    { "size",   required_argument, 0, 's' },
    { "default",required_argument, 0, 'd' },
    { "epoch",  required_argument, 0, 'e' },
    {}
};

char*   opt_input_file     = nullptr;
bool    opt_all            = false;
bool    opt_rus            = false;

int     opt_funcs_idx[MAX_FUNCS_AMOUNT]    = {};
int     opt_funcs_amount                   = 0;

size_t  opt_htable_size                    = 0;
size_t  opt_lst_capacity                   = 0;

size_t  opt_epochs                         = 0;

int parse_args( int argc, char* const argv[] )
{
    while( true )
    {
        int val = getopt_long( argc, argv, optstring, program_options, nullptr );
        if( val == -1 ) break;
        
        switch ( val )
        {
            case 'f':   // --file=[input file name]
            {
                opt_input_file  = optarg;
                break;
            }
            case 'h':   // --hash=[hash function idx(s) {0-6}]
            {
                if( opt_all ) break;
                
                size_t  len = strlen( optarg );
                for( size_t i = 0; i < len; ++i )
                {
                    int arr_idx  = opt_funcs_amount % MAX_FUNCS_AMOUNT;
                    int func_idx = optarg[i] - '0';
                    
                    if( !( func_idx >= 0 && func_idx < HASH_FUNCS_AMOUNT ) )
                    {
                        fprintf( stderr, "Unknown hash func specified" );
                        return EXIT_ARG_ERR;
                    }
                    
                    opt_funcs_idx[arr_idx] = func_idx;
                    opt_funcs_amount++;
                }
                break;
            }
            case 'a': // --all - use all known hash funcs
            {
                opt_all = true;
                
                for( int i = 0; i < HASH_FUNCS_AMOUNT; i++ )
                    opt_funcs_idx[i] = i;
                
                opt_funcs_amount = HASH_FUNCS_AMOUNT;
                
                break;
            }
            case 'r':
            {
                opt_rus = true;
                break;
            }
            case 's':
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Size?\n" );
                    return EXIT_ARG_ERR;
                }
                else
                    sscanf( optarg, "%zd", &opt_htable_size );
                break;
            }
            case 'd':
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Capacity?\n" );
                    return EXIT_ARG_ERR;
                }
                else
                    sscanf( optarg, "%zd", &opt_lst_capacity );
                break;
            }
            case 'e':
            {
                if( optarg == nullptr )
                {
                    fprintf( stderr, "Epochs?\n" );
                    return EXIT_ARG_ERR;
                }
                else
                    sscanf( optarg, "%zd", &opt_epochs );
                break;
            }
            default:
            {
                fprintf( stderr, "Unexpected option\n" );
                fprintf( stderr, "%s", usage );
                return EXIT_ARG_ERR;
            }
        }
    }
    
    if( opt_input_file == nullptr )
    {
        fprintf( stderr, "No input file specified\n" );
        return EXIT_ARG_ERR;
    }
    
    if( opt_funcs_amount == 0 )
    {
        fprintf( stderr, "No funcs specified\n" );
        return EXIT_ARG_ERR;
    }
    
    return 0;
}
