#include <stdio.h>
#include <cstring>
#include <cassert>
#include "hash_table.h"
#include "posixint.h"

char**  get_words_buf( FILE* file );

int main( int argc, char* const argv[] )
{
    if( argc == 0 )
    {
        printf( "%s", usage );
        return 0;
    }
    
    int parser_result = parse_args( argc, argv );
    if( parser_result != 0 )
        return parser_result;
    
    FILE*   input_file = fopen( opt_input_file, "r" );
    
    if( ferror( input_file ) != 0 )
    {
        fprintf( stderr, "Error opening input file\n" );
        return EXIT_INFILE_ERR;
    }

    char**  word_ptr_buf = get_words_buf( input_file );
    fclose( input_file );
    
    printf( "Filling tables... " );
    hash_table**     tables = (hash_table**)calloc( sizeof(hash_table*), opt_funcs_amount );
    
    assert( tables != nullptr );
    for( size_t i = 0; i < opt_funcs_amount; ++i )
    {
        tables[i] = construct_htable( opt_htable_size ? opt_htable_size : DEFAULT_HTABLE_SIZE,
                                     hash_funcs[opt_funcs_idx[i]],
                                     opt_lst_capacity ? opt_lst_capacity : DEFAULT_HTABLE_SIZE );
    }
    
    char    csv_filename[MAX_FILENAME_LEN] = {};
    
    for( size_t i = 0; i < opt_funcs_amount; ++i )
    {
        for( size_t j = 1; word_ptr_buf[j] != nullptr; ++j )
        {
            push_str( tables[i], word_ptr_buf[j] );
        }
        sprintf( csv_filename, "hash%zd.csv", i );
        
        FILE*   fout = fopen( csv_filename, "w" );
        assert( ferror(fout) == 0 );
        
        csv_dump( tables[i], fout );
        
        fclose( fout );
    }
    
    printf( "done\n" );
    
    if( opt_epochs != 0 )
    {
        printf( "Test started, epochs %zd\n", opt_epochs );
        
        for( size_t ep = 0; ep < opt_epochs; ++ep )
        {
            printf( "\tepoch %zd\n", ep + 1 );
            for( size_t i = 0; i < opt_funcs_amount; ++i )
            {
                printf( "\t\ttest%zd... ", i );
                for( size_t j = 1; word_ptr_buf[j] != nullptr; ++j )
                {
                    contains( tables[i], word_ptr_buf[j] );
                }
                printf( " done\n" );
            }
        }
        printf( "Test finished\n" );
    }
    
    printf( "Destructing tables... " );
    for( size_t i = 0; i < opt_funcs_amount; ++i )
    {
        destruct_htable( tables[i] );
    }
    free( tables );
    free( word_ptr_buf );
    
    printf( "done\n" );
    
    return 0;
}

long file_size( FILE* file )
{
    assert( nullptr != file );
    assert( ferror( file ) == 0 );
    
    long currentPosition = ftell( file );
    
    fseek( file, 0, SEEK_END );
    long size  = ftell( file );
    
    fseek( file, currentPosition, SEEK_SET );
    
    return size;
}

char**  get_words_buf( FILE* file )
{
    long    buf_size = file_size( file ) + 1;
    char*   buffer   = (char*)calloc( buf_size, sizeof( char ) );
    
    if( buffer == nullptr )
    {
        fprintf( stderr, "Memory error\n" );
        return nullptr;
    }
    
    fread( buffer, sizeof( char ), buf_size, file );
    
    long res_size = buf_size + 10;
    
    char**  word_ptr_buf = (char**)calloc( sizeof( char* ), res_size );
    
    if( word_ptr_buf == nullptr )
    {
        fprintf( stderr, "Memory error\n" );
        free( buffer );
        return nullptr;
    }
    
    char*   capletters = opt_rus ? (char*)"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" :
                                   (char*)"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    char*   letters    = opt_rus ? (char*)"абвгдеёжзийклмнопрстуфхцчшщъыьэюя" :
                                   (char*)"abcdefghijklmnopqrstuvwxyz";
    
    char    shift      = opt_rus ? 0 : capletters[0] - letters[0];
    
    *word_ptr_buf = buffer;
    
    // kill all dumb chars and lowercase text
    for( char* killer = buffer; killer < buffer + buf_size; ++killer )
    {
        if( strchr( capletters, *killer ) != nullptr )
            *killer -= shift;
        if( strchr( letters, *killer ) == nullptr )
            *killer = '\0';
    }
    
    char**  r_ptr = word_ptr_buf + 1;
    
    for( char* b_ptr = buffer; b_ptr < buffer + buf_size; b_ptr++ )
    {
        if( *b_ptr != '\0' )
            *(r_ptr++) = b_ptr;
        
        while( *b_ptr != '\0' && b_ptr < buffer + buf_size )
            b_ptr++;
    }
    
    printf( "Precompilation done %d words\n", (int)(r_ptr - word_ptr_buf) );
    return word_ptr_buf;
}
