#ifndef posixint_h
#define posixint_h

#include <getopt.h>

#define MAX_FILENAME_LEN    100

#define MAX_FUNCS_AMOUNT    100

#define EXIT_ARG_ERR        100
#define EXIT_INFILE_ERR     101
#define EXIT_MEMORY_ERR     102

extern char    usage[];

extern char    optstring[];

extern option  program_options[];

extern char*   opt_input_file;
extern bool    opt_all;
extern bool    opt_rus;

extern int     opt_funcs_idx[];
extern int     opt_funcs_amount;

extern size_t  opt_htable_size;
extern size_t  opt_lst_capacity;

extern size_t  opt_epochs;

extern int     parse_args( int argc, char* const argv[] );


#endif /* posixint_h */
